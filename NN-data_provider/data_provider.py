from builtins import print, len, range, str
import pandas as pd
from matplotlib import pyplot as plt
import numpy as np

smoothing = 2
sma_index = 30
ema_symblol = 14
sma_symbol = 15


def ema_adder(data_frame, ema_period, key_word='ema'):
    closes = data_frame['last_price']
    ema = []
    for j in range(0, len(closes)):
        if j < ema_period - 1:
            ema.append(np.nan)
        elif j == ema_period - 1:
            ema.append(np.mean(closes[:ema_period]))
        else:
            ema.append(closes[j] * (smoothing / (ema_period + 1)) + ema[j - 1] * (1 - (smoothing / (ema_period + 1))))
    data_frame[key_word + str(ema_period)] = ema
    return data_frame


def sma_adder(data_frame, sma_period, key_word='sma'):
    indexx = df['indexx'].tolist()
    sma = []
    q = 0
    while q < len(indexx):
        if q < sma_period - 1:
            sma.append(np.nan)
        elif q == sma_period - 1:
            sma.append(np.mean(indexx[:sma_period]))
        else:
            sma.append(sma[q - 1] - indexx[q - 8] / sma_period + indexx[q] / sma_period)
        q = q + 1
    df[key_word + str(sma_period)] = sma
    return df


stock_paths = {
    'symbol': ['پارس', 'پارسان', 'تاپیکو', 'حکشتی', 'رمپنا', 'شبندر', 'فارس', 'فملی',
               'فولاد', 'کچاد', 'کگل', 'کگهر', 'وامید', 'وبملت', 'وغدیر'],

    'path': [r'C:\Users\hd\Desktop\New folder (3)\challenge\پارس.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\پارسان.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\تاپیکو.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\حکشتی.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\رمپنا.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\شبندر.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\فارس.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\فملی.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\فولاد.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\کچاد.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\کگل.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\کگهر.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\وامید.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\وبملت.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\وغدیر.xlsx']
}
df_stock_paths = pd.DataFrame(stock_paths)

nn_dataset_paths = {
    'symbol': ['NNپارس', 'پارسانNN', 'تاپیکوNN', 'حکشتیNN', 'رمپناNN', 'شبندرNN', 'فارسNN', 'فملیNN',
               'فولادNN', 'کچادNN', 'کگلNN', 'کگهرNN', 'وامیدNN', 'وبملتNN', 'وغدیرNN'],

    'path': [r'C:\Users\hd\Desktop\New folder (3)\challenge\NNپارس.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\NNپارسان.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\NNتاپیکو.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\NNحکشتی.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\NNرمپنا.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\NNشبندر.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\NNفارس.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\NNفملی.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\NNفولاد.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\NNکچاد.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\NNکگل.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\NNکگهر.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\NNوامید.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\NNوبملت.xlsx',
             r'C:\Users\hd\Desktop\New folder (3)\challenge\NNوغدیر.xlsx']
}
df_nn_dataset_paths = pd.DataFrame(nn_dataset_paths)


#   adding sma index to data
for path in df_stock_paths['path'].tolist():
    df = pd.read_excel(path)
    if not ('sma_index' + str(sma_index) + 'index' in df):
        df = sma_adder(df, sma_index, 'sma_index')
        df.to_excel(path, index=False)

#   adding target to data
for path in df_stock_paths['path'].tolist():
    df = pd.read_excel(path)
    return_to_index = df['ret_wrt_index'].tolist()
    length = len(return_to_index)
    target = []
    i = 0
    if not ('target' in df):
        for x in return_to_index:
            if x > 0.25:
                target.append(1)
            elif 0.18 < x <= 0.25:
                target.append(2)
            elif 0.12 < x <= 0.18:
                target.append(3)
            elif 0.06 < x <= 0.12:
                target.append(4)
            elif 0 < x <= 0.06:
                target.append(5)
            else:
                target.append(6)
        df['target'] = target
        df.to_excel(path, index=False)

#   adding diff to data
for path in df_stock_paths['path'].tolist():
    df = pd.read_excel(path)
    close = df['last_price'].tolist()
    diff = []
    for i in range (len(close)):
        if i == 0:
            diff.append(close[i])
        else:
            diff.append(close[i]-close[i-1])
    df['diff'] = diff
    df.to_excel(path, index=False)


#   adding ema symbols to data
for path in df_stock_paths['path'].tolist():
    ema_symbol = 14
    df = pd.read_excel(path)
    if not ('ema' + str(ema_symbol) in df):
        df = ema_adder(df, ema_symbol)
        df.to_excel(path, index=False)

# creating dataset with custom features to feed neural network
i = 0
stock_paths = df_stock_paths['path'].tolist()
dataset_pats = df_nn_dataset_paths['path'].tolist()
while i < len(stock_paths):
    my_data_frame = pd.DataFrame()
    df = pd.read_excel(stock_paths[i])
    #    my_data_frame['I_buy_sarane'] = df['I_buy_sarane']
    my_data_frame['diff'] = df['diff']
    my_data_frame['volume'] = df['volume']
    my_data_frame['I_buy_power_avg'] = df['Avg_Buy_Ind_per_capita'] / df['Avg_Sell_Ind_per_capita']
    my_data_frame['index>sma'] = df['indexx'] > df['sma_index30']
    my_data_frame['price>ema'] = df['last_price'] > df['ema14']
    #    my_data_frame['Avg_value_30'] = df['Avg_value_30']
    my_data_frame['%Sell_Non_Ind'] = df['%Sell_Non_Ind']
    #    my_data_frame['sma8'] = df['sma8']
    #    my_data_frame['sma_index20'] = df['sma_index20']
    #    my_data_frame['effective_day'] = df['effective_day']
    my_data_frame['target'] = df['target']
    my_data_frame.to_excel(dataset_pats[i], index=False)
    i = i + 1
