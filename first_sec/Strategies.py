from builtins import int, print, sum, len, range, str
import pandas as pd
import numpy as np
from data_processing import IndicatorAdder, PivotFinder


def rsi_pivot_strategy(path, rsi_period=14, cci_period=14, mfi_period=14):
    df = pd.read_excel(path)
    df = IndicatorAdder(df, rsi_period, cci_period, mfi_period, path=path)
    df = PivotFinder(df)
    close = df['last_price'].tolist()
    rsi_values = df['rsi' + str(rsi_period)].tolist()
    down_pivots = df['down_pivot'].tolist()
    return_to_index = df['ret_wrt_index'].tolist()
    dates = df['date'].tolist()
    obv_values = df['obv'].tolist()
    index_daily_return = df['ret1D_index'].tolist()
    symbol_daily_return = df['ret1D'].tolist()

    num_down_pivots = down_pivots.count(True)
    i = 0
    last_index = 0
    pivot_index = []
    while i < num_down_pivots:
        index = down_pivots.index(True, last_index + 1)
        pivot_index.append(index)
        last_index = index
        i += 1

    num_trades = 0
    num_profit = 0
    num_loss = 0
    trade_returns_to_index = []
    trade_dates = []

    for i in pivot_index:
        index = pivot_index.index(i)
        if index > 1 and rsi_values[i] < rsi_values[pivot_index[index - 1]] and close[i] > close[
            pivot_index[index - 1]]:
            if return_to_index[i] > 0:
                num_profit += 1
            else:
                num_loss += 1
            trade_dates.append(dates[i + 6])
            if i + 6 < len(dates) - 31:
                trade_returns_to_index.append(return_to_index[i + 6])
            else:
                trade_returns_to_index.append(return_to_index[len(dates) - 31])
    num_trades = num_profit + num_loss
    return [num_trades, num_profit, num_loss, trade_dates, trade_returns_to_index]


def rsi_simple_strategy(rsi_period, path):
    df = pd.read_excel(path)
    df = IndicatorAdder(df, rsi_period, path=path)
    close = df['last_price'].tolist()
    rsi_values = df['rsi' + np.str(rsi_period)].tolist()
    return_to_index = df['ret_wrt_index'].tolist()
    dates = df['date'].tolist()
    obv_values = df['obv'].tolist()
    index_daily_return = df['ret1D_index'].tolist()
    symbol_daily_return = df['ret1D'].tolist()

    num_trades = 0
    num_profit = 0
    num_loss = 0
    trade_returns_to_index = []
    trade_dates = []
    i = rsi_period + 1
    net_profit = 0

    while i < len(close) - 31:

        if rsi_values[i] < 30 and rsi_values[i] > 20 and close[i + 1] > close[i]:
            # applying 10% stoploss
            if np.min(close[i:i + 30]) < 0.9 * close[i]:
                for index in range(i, i + 30):
                    if close[index] > 0.9 * close[i]:
                        continue
                    else:
                        stop_loss_index = index
                        break
                net_profit = sum(symbol_daily_return[i:stop_loss_index]) - 0.5 * sum(
                    index_daily_return[i:stop_loss_index])
            else:
                net_profit = return_to_index[i]
            #
            if net_profit > 0:
                num_profit += 1
            else:
                num_loss += 1
            trade_returns_to_index.append(net_profit)
            trade_dates.append(dates[i])
        i += 1

    num_trades = num_profit + num_loss
    return [num_trades, num_profit, num_loss, trade_dates, trade_returns_to_index]


def mfi_simple_strategy(mfi_period, path):
    df = pd.read_excel(path)
    df = IndicatorAdder(df, mfi_period=mfi_period, path=path)
    close = df['last_price'].tolist()
    mfi_values = df['mfi' + np.str(mfi_period)].tolist()
    return_to_index = df['ret_wrt_index'].tolist()
    dates = df['date'].tolist()
    obv_values = df['obv'].tolist()
    index_daily_return = df['ret1D_index'].tolist()
    symbol_daily_return = df['ret1D'].tolist()

    num_trades = 0
    num_profit = 0
    num_loss = 0
    trade_returns_to_index = []
    trade_dates = []
    i = mfi_period + 1
    while i < len(close) - 31:
        if mfi_values[i] < 20 and close[i] > close[i - 1]:

            # applying 10% stoploss
            if np.min(close[i:i + 30]) < 0.9 * close[i]:
                for index in range(i, i + 30):
                    if close[index] > 0.9 * close[i]:
                        continue
                    else:
                        stop_loss_index = index
                        break
                net_profit = sum(symbol_daily_return[i:stop_loss_index]) - 0.5 * sum(
                    index_daily_return[i:stop_loss_index])
            else:
                net_profit = return_to_index[i]
            #
            if net_profit > 0:
                num_profit += 1
            else:
                num_loss += 1
            trade_returns_to_index.append(net_profit)
            trade_dates.append(dates[i])
        i += 1

    num_trades = num_loss + num_profit
    return [num_trades, num_profit, num_loss, trade_dates, trade_returns_to_index]


def cci_simple_strategy(cci_period, path):
    df = pd.read_excel(path)
    df = IndicatorAdder(df, cci_period=cci_period, path=path)
    close = df['last_price'].tolist()
    cci_values = df['cci' + str(cci_period)].tolist()
    return_to_index = df['ret_wrt_index'].tolist()
    dates = df['date'].tolist()
    obv_values = df['obv'].tolist()
    index_daily_return = df['ret1D_index'].tolist()
    symbol_daily_return = df['ret1D'].tolist()

    num_trades = 0
    num_profit = 0
    num_loss = 0
    trade_returns_to_index = []
    trade_dates = []
    i = cci_period + 1
    while i < len(close) - 31:
        if cci_values[i - 1] < -120 and cci_values[i] > -115:
            # applying 10% stoploss
            if np.min(close[i:i + 30]) < 0.9 * close[i]:
                for index in range(i, i + 30):
                    if close[index] > 0.9 * close[i]:
                        continue
                    else:
                        stop_loss_index = index
                        break
                net_profit = sum(symbol_daily_return[i:stop_loss_index]) - 0.5 * sum(
                    index_daily_return[i:stop_loss_index])
            else:
                net_profit = return_to_index[i]
            #
            if net_profit > 0:
                num_profit += 1
            else:
                num_loss += 1
            trade_returns_to_index.append(net_profit)
            trade_dates.append(dates[i])
        i += 1

    num_trades = num_loss + num_profit
    return [num_trades, num_profit, num_loss, trade_dates, trade_returns_to_index]


def mixed_strategy(path, exit_line, mfi_period, obv_flag):
    df = pd.read_excel(path)
    df = IndicatorAdder(df, mfi_period=mfi_period, cci_period=100)
    cci_values = df['cci100']
    obv_values = df['obv']
    mfi_values = df['mfi' + str(mfi_period)]
    close = df['last_price'].tolist()
    trading_flag = False
    index_daily_return = df['ret1D_index'].tolist()
    symbol_daily_return = df['ret1D'].tolist()
    dates = df['date'].tolist()

    num_trades = 0
    num_profit = 0
    num_loss = 0
    trade_returns_to_index = []
    trade_dates = []

    if obv_flag:
        for i in range(mfi_period, len(close)):
            if not trading_flag and cci_values[i] > 100 and mfi_values[i] > 80 \
                    and obv_values[i] > obv_values[i-mfi_period]:
                trading_flag = True
                start = i
                num_trades += 1
                trade_dates.append(dates[i])
            if mfi_values[i] < exit_line and trading_flag:
                end = i
                trading_flag = False
                profit = sum(symbol_daily_return[start:end]) - 0.5 * sum(
                    index_daily_return[start:end])
                if profit > 0:
                    num_profit += 1
                else:
                    num_loss += 1
                trade_returns_to_index.append(profit)
        return [num_trades, num_profit, num_loss, trade_dates, trade_returns_to_index]

    if not obv_flag:
        for i in range(mfi_period, len(close)):
            if not trading_flag and cci_values[i] > 100 and mfi_values[i] > 80:
                trading_flag = True
                start = i
                num_trades += 1
                trade_dates.append(dates[i])
            if mfi_values[i] < exit_line and trading_flag:
                end = i
                trading_flag = False
                profit = sum(symbol_daily_return[start:end]) - 0.5 * sum(
                    index_daily_return[start:end])
                if profit > 0:
                    num_profit += 1
                else:
                    num_loss += 1
                trade_returns_to_index.append(profit)
        return [num_trades, num_profit, num_loss, trade_dates, trade_returns_to_index]


