from builtins import int, print, sum

import pandas as pd
from geneal.genetic_algorithms import ContinuousGenAlgSolver, BinaryGenAlgSolver
from Strategies import rsi_simple_strategy, rsi_pivot_strategy, mfi_simple_strategy, cci_simple_strategy, mixed_strategy

stock_paths = {
    'symbol': ['پارس', 'پارسان', 'تاپیکو', 'حکشتی', 'رمپنا', 'شبندر', 'فارس', 'فملی',
               'فولاد', 'کچاد', 'کگل', 'کگهر', 'وامید', 'وبملت', 'وغدیر'],

    'path': [r'C:\Users\hd\Desktop\New folder (2)\challenge\پارس.xlsx',
             r'C:\Users\hd\Desktop\New folder (2)\challenge\پارسان.xlsx',
             r'C:\Users\hd\Desktop\New folder (2)\challenge\تاپیکو.xlsx',
             r'C:\Users\hd\Desktop\New folder (2)\challenge\حکشتی.xlsx',
             r'C:\Users\hd\Desktop\New folder (2)\challenge\رمپنا.xlsx',
             r'C:\Users\hd\Desktop\New folder (2)\challenge\شبندر.xlsx',
             r'C:\Users\hd\Desktop\New folder (2)\challenge\فارس.xlsx',
             r'C:\Users\hd\Desktop\New folder (2)\challenge\فملی.xlsx',
             r'C:\Users\hd\Desktop\New folder (2)\challenge\فولاد.xlsx',
             r'C:\Users\hd\Desktop\New folder (2)\challenge\کچاد.xlsx',
             r'C:\Users\hd\Desktop\New folder (2)\challenge\کگل.xlsx',
             r'C:\Users\hd\Desktop\New folder (2)\challenge\کگهر.xlsx',
             r'C:\Users\hd\Desktop\New folder (2)\challenge\وامید.xlsx',
             r'C:\Users\hd\Desktop\New folder (2)\challenge\وبملت.xlsx',
             r'C:\Users\hd\Desktop\New folder (2)\challenge\وغدیر.xlsx']
}
df_paths = pd.DataFrame(stock_paths)


#   use this function for rsi_simple strategy , mfi_simple_strategy , cci_simple_strategy and rsi_pivot_strategy
def fitness_function1(chromosome):
    print(chromosome)
    period = int(chromosome[0] * 8 + chromosome[1] * 4 + chromosome[2] * 2 + chromosome[3] + 5)
    score = 0
    num_trade = 0
    for path in df_paths['path'].tolist():
        results = rsi_pivot_strategy(rsi_period=period, path=path)
        if results[0] > 0:
            num_trade = results[0] + num_trade
            score = sum(results[4]) + score
    print(score, num_trade)
    return score / num_trade


#   use this function for mixed strategy
def fitness_function2(chromosome):
    print(chromosome)
    exit_line = int(chromosome[3] + chromosome[4] * 2 + chromosome[5] * 4 + chromosome[6] * 8 + 40)
    mfi_period = int(chromosome[0] + chromosome[1] * 2 + chromosome[2] * 4 + 10)
    obv_flag = True if chromosome[7] == 1 else False
    score = 0
    num_trade = 0
    for path in df_paths['path'].tolist():
        results = mixed_strategy(path, exit_line, mfi_period, obv_flag)
        if results[0] > 0:
            num_trade = results[0] + num_trade
            score = sum(results[4]) + score
    print(score, num_trade)
    return score / num_trade


solver = BinaryGenAlgSolver(
    n_genes=8,
    fitness_function=fitness_function1,
    pop_size=20,  # population size (number of individuals)
    max_gen=20,  # maximum number of generations
    mutation_rate=0.2,  # mutation rate to apply to the population
    selection_rate=0.4,  # percentage of the population to select for mating
    selection_strategy="roulette_wheel",  # strategy to use for selection
)

solver.solve()
