from builtins import len, str
import numpy as np
from ta.momentum import rsi
from ta.trend import cci
from ta.volume import money_flow_index
from ta.volume import on_balance_volume
import statistics as stats


#   adding indicators to database

def IndicatorAdder(df, rsi_period=14, cci_period=14, mfi_period=14, path=r'C:\Users\hd\Desktop\New folder ('
                                                                         r'2)\challenge\شبندر.xlsx'):
    # adding rsi , mfi , obv and cci to data
    if not (('rsi' + str(rsi_period)) in df):
        rsi_values = rsi(df['last_price'], rsi_period, False).tolist()
        df['rsi' + str(rsi_period)] = rsi_values

    if not ('obv' in df):
        obv_values = on_balance_volume(df['last_price'], df['volume'], True).tolist()
        df['obv'] = obv_values

    if not (('cci' + str(cci_period)) in df):
        cci_values = cci(df['max_price'], df['min_price'], df['last_price'], cci_period, 0.015, False).tolist()
        df['cci' + str(cci_period)] = cci_values

    if not (('mfi' + str(mfi_period)) in df):
        mfi_values = money_flow_index(df['max_price'], df['min_price'], df['last_price'], df['volume'], mfi_period,
                                      True).tolist()
        df['mfi' + str(mfi_period)] = mfi_values
    df.to_excel(path, index=False)
    return df


# finding pivots and if founded, changing the value of top_pivot/down_pivot to true

def PivotFinder(df):
    df = df

    df['top_pivot'] = False
    df['down_pivot'] = False
    top_pick = df['top_pivot'].tolist()
    down_pick = df['down_pivot'].tolist()
    i = 15
    closes = df['last_price'].tolist()
    maximun_ratio = 0.9

    while i < len(closes) - 8:
        if np.max(closes[i - 15:i + 6]) == closes[i] and (np.min(closes[i + 1:i + 6]) / closes[i]) < maximun_ratio:
            top_pick[i] = True
            i = i + 5
        if np.min(closes[i - 6:i + 8]) == closes[i] and (closes[i] / np.max(closes[i + 1:i + 8])) < maximun_ratio:
            down_pick[i] = True
            i = i + 5
        i = i + 1
    df['top_pivot'] = top_pick
    df['down_pivot'] = down_pick
    return df
