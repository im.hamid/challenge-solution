from builtins import print, range, len
import pandas as pd
import numpy as np
import keras.backend as K
from sklearn.preprocessing import MinMaxScaler
from sklearn.ensemble import RandomForestClassifier
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from sklearn.metrics import confusion_matrix
import keras
import matplotlib as plt


# one hot encoder
def encoder(Y_train):
    Y_train_encoded = []
    for element in Y_train:
        if element == 5:
            Y_train_encoded.append([0.04, 0.06, 0.12, 0.18, 0.5, 0.10])
        elif element == 1:
            Y_train_encoded.append([0.95, 0.03, 0.02, 0, 0, 0])
        elif element == 2:
            Y_train_encoded.append([0.05, 0.91, 0.03, 0.01, 0, 0])
        elif element == 3:
            Y_train_encoded.append([0.09, 0.12, 0.75, 0.03, 0.01, 0])
        elif element == 4:
            Y_train_encoded.append([0.06, 0.09, 0.14, 0.68, 0.03, 0])
        else:
            Y_train_encoded.append([0.05, 0.07, 0.08, 0.10, 0.20, 0.5])

    Y_train_encoded = np.array(Y_train_encoded)
    return Y_train_encoded


# metric of imbalanced classification
def get_f1(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    recall = true_positives / (possible_positives + K.epsilon())
    f1_val = 2 * (precision * recall) / (precision + recall + K.epsilon())
    return f1_val


# reshaping inputs
def xy_shaper(x, y):
    X = []
    Y = []
    for j in range(time_step, x.shape[0]):
        X.append(x[j - time_step:j, :])
        Y.append(y[j])
    X = np.array(X)
    Y = np.array(Y)
    X = np.reshape(X, (X.shape[0], X.shape[1], num_features))
    return X, Y


#   preparing data
sc = MinMaxScaler(feature_range=(0, 1))

time_step = 10
num_features = 6

# paste your path here
df = pd.read_excel(r'C:\Users\hd\Desktop\New folder (4)\NNفولاد.xlsx')

my_data = np.array(df)

pre_x_matrix = my_data[:my_data.shape[0], :num_features]
pre_y_matrix = my_data[:my_data.shape[0], num_features]

#   normalizing inputs
pre_x_matrix = sc.fit_transform(pre_x_matrix)

train_ratio = 0.9
data_lenght = np.int((pre_x_matrix.shape[0]) * train_ratio)

pre_x_train = pre_x_matrix[:data_lenght, :]
pre_x_test = pre_x_matrix[data_lenght:, :]
pre_y_train = pre_y_matrix[:data_lenght]
pre_y_test = pre_y_matrix[data_lenght:]

X_train, Y_train = xy_shaper(pre_x_train, pre_y_train)
X_test, Y_test = xy_shaper(pre_x_test, pre_y_test)

#   building lstm neural network model
model = Sequential()
model.add(LSTM(units=6, return_sequences=True, input_shape=(X_train.shape[1], num_features)))
model.add(Dropout(0.2))
model.add(LSTM(units=6, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(units=6, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(units=6))
model.add(Dropout(0.2))
model.add(Dense(units=6, activation='softmax'))

model.compile(optimizer='adam', loss='kullback_leibler_divergence', metrics=['accuracy', get_f1])

model.fit(X_train, encoder(Y_train), epochs=40, batch_size=80, validation_data=(X_test, encoder(Y_test)))
pre_y_predicted = model.predict(X_test)
pre_y_predicted = pre_y_predicted.tolist()
y_predicted = []

for y in pre_y_predicted:
    y_predicted.append(y.index(np.max(y)) + 1)

print(confusion_matrix(Y_test, y_predicted))
